// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
// 4. Create a countStudents() function that will print the total number of students in the array.
// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
// 6. Create a findStudent() function that will do the following:
// Search for a student name when a keyword is given (filter method).
// If one match is found print the message studentName is an enrollee.
// If multiple matches are found print the message studentNames are enrollees.
// If no match is found print the message studentName is not an enrollee.
// The keyword given should not be case sensitive.


// 3.
let students = [];

function addStudent(studentName){
	students.push(studentName);
	console.log(studentName + " was added to the student's list")

}


// 4.
function countStudents(studentName) {
	console.log(students.length);
}


// 5.
function printStudents(){
	students.sort(function(a, b) {
		return a.localeCompare(b);
	});

	students.forEach(function(student){

		console.log(student);
	})
}


// 6.
function findStudent(studentName){
	let result = students.filter(function(student){
		return student.includes(studentName);
	});

	if (result.length === 1) {
		console.log(studentName + " is an enrollee")
	}
	else if (result.length > 1){
		console.log(students + " are enrollees");
	}
	else {
		console.log(studentName + " is not an enrollee.");
	}
}



// 
function addSection(section){
	let studentSection = students.map(function(student){
		return students + "-" + section;
	})

	console.log(studentSection);
}

function removeStudent(studentName){
	console.log(studentName.charAt(0).toUpperCase() + studentName.slice(1))
	let studentIndex = students.indexOf(studentName);
	console.log("Index of student is" +studentIndex);
	students.splice(studentIndex, 1);
	console.log(studentName + " was removed from the student's list");


}


